<?php

/**
 * Displays header site banner area
 *
 * @package WordPress
 * @subpackage Berry_Good_Farms
 * @since 1.0.0
 */
?>
<?php if (get_theme_mod('banner_image') || get_theme_mod('banner_slider') ||  get_theme_mod('header_title') || get_theme_mod('hd_text')) : ?>
	<div class="site-banner">
		<div class="banner-text">
			<?php if (get_theme_mod('banner_slider')) :

					$s_code = get_theme_mod('banner_slider');
					echo do_shortcode($s_code);

				else :

					$link_one = get_theme_mod('header_btn_one_link') ?: '#';
					$link_two = get_theme_mod('header_btn_two_link') ?: '#';

					if (get_theme_mod('banner_image')) :
						echo '<div class="img-holder"><img src="' . get_theme_mod('banner_image') . '" alt="image"></div>';
					endif; ?>


			<?php endif; ?>

			<div class="wrapper">
				<!-- <div class="logo-holder"><?php the_custom_logo(); ?></div> -->
				<div class="bw-text">
					<div class="bw-top">
						<?php get_template_part('template-parts/navigation/navigation', 'social'); ?>
						<?php echo get_theme_mod('header_sub_title') ? '<h2 class="social-title">' . get_theme_mod('header_sub_title') . '</h2>' : ''; ?>
					</div>
					<?php echo get_theme_mod('header_title') ? '<h2 class="title">' . get_theme_mod('header_title') . '</h2>' : ''; ?>
					<?php if (get_bloginfo(description)) : ?>
						<p><?php echo get_bloginfo(description); ?></p>
					<?php endif; ?>
					<!-- <?php //if (get_theme_mod('header_btn_one_text') || get_theme_mod('header_btn_two_text')) : 
								?>
					<div class="link">
						<?php	// echo get_theme_mod('header_btn_one_text') ? '<a href="' . $link_one . '" class="btn">' . get_theme_mod('header_btn_one_text') . '</a>' : ''; 
							?>
						<?php //echo get_theme_mod('header_btn_two_text') ? '<a href="' . $link_two . '" class="btn btn-black">' . get_theme_mod('header_btn_two_text') . '</a>' : ''; 
							?>
					</div>
					<?php //endif; 
						?> -->
				</div>
			</div>
		</div>

	</div><!-- .site-banner -->
<?php endif; ?>