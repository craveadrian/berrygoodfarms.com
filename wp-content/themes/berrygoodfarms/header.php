<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Berry_Good_Farms
 * @since 1.0.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'berrygoodfarms' ); ?></a>

	<header id="masthead" class="site-header">

		<div class="site-branding-container container">
			<?php //get_template_part( 'template-parts/header/site', 'branding' ); ?>
			<div class="hd-wrap">
				<?php if ( has_custom_logo() ) : ?>
					<div class="hd-logo"><?php the_custom_logo(); ?></div>
				<?php endif; ?>
				<div class="header-nav">
					<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
				</div>
			</div>
		</div><!-- .layout-wrap -->

	</header><!-- #masthead -->
	<?php if ( is_home() || is_front_page() ) :?>
		<?php get_template_part( 'template-parts/header/site', 'banner' ); ?>
	<?php endif; ?>
	<div id="content" class="site-content">
