<?php
/**
 * Berry Good Farms back compat functionality
 *
 * Prevents Berry Good Farms from running on WordPress versions prior to 4.7,
 * since this theme is not meant to be backward compatible beyond that and
 * relies on many newer functions and markup changes introduced in 4.7.
 *
 * @package WordPress
 * @subpackage Berry_Good_Farms
 * @since Berry Good Farms 1.0.0
 */

/**
 * Prevent switching to Berry Good Farms on old versions of WordPress.
 *
 * Switches to the default theme.
 *
 * @since Berry Good Farms 1.0.0
 */
function berrygoodfarms_switch_theme() {
	switch_theme( WP_DEFAULT_THEME );
	unset( $_GET['activated'] );
	add_action( 'admin_notices', 'berrygoodfarms_upgrade_notice' );
}
add_action( 'after_switch_theme', 'berrygoodfarms_switch_theme' );

/**
 * Adds a message for unsuccessful theme switch.
 *
 * Prints an update nag after an unsuccessful attempt to switch to
 * Berry Good Farms on WordPress versions prior to 4.7.
 *
 * @since Berry Good Farms 1.0.0
 *
 * @global string $wp_version WordPress version.
 */
function berrygoodfarms_upgrade_notice() {
	$message = sprintf( __( 'Berry Good Farms requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'berrygoodfarms' ), $GLOBALS['wp_version'] );
	printf( '<div class="error"><p>%s</p></div>', $message );
}

/**
 * Prevents the Customizer from being loaded on WordPress versions prior to 4.7.
 *
 * @since Berry Good Farms 1.0.0
 *
 * @global string $wp_version WordPress version.
 */
function berrygoodfarms_customize() {
	wp_die(
		sprintf(
			__( 'Berry Good Farms requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'berrygoodfarms' ),
			$GLOBALS['wp_version']
		),
		'',
		array(
			'back_link' => true,
		)
	);
}
add_action( 'load-customize.php', 'berrygoodfarms_customize' );

/**
 * Prevents the Theme Preview from being loaded on WordPress versions prior to 4.7.
 *
 * @since Berry Good Farms 1.0.0
 *
 * @global string $wp_version WordPress version.
 */
function berrygoodfarms_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die( sprintf( __( 'Berry Good Farms requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'berrygoodfarms' ), $GLOBALS['wp_version'] ) );
	}
}
add_action( 'template_redirect', 'berrygoodfarms_preview' );
