<?php
/**
 * WP Default Custom Post Types Sample
 *
 * @package WP_Default
 */

/**
 * Create fence post type
 */
function fence_post_type() {

	$labels = array(
		'name'					=> __( 'Fences', 'berrygoodfarms' ),
		'singular_name'			=> __( 'Fence', 'berrygoodfarms' ),
		'add_new'				=> __( 'New Fence', 'berrygoodfarms' ),
		'add_new_item'			=> __( 'Add New Fence', 'berrygoodfarms' ),
		'edit_item'				=> __( 'Edit Fence', 'berrygoodfarms' ),
		'new_item'				=> __( 'New Fence', 'berrygoodfarms' ),
		'view_item'				=> __( 'View Fence', 'berrygoodfarms' ),
		'search_items'			=> __( 'Search Fences', 'berrygoodfarms' ),
		'not_found'				=>  __( 'No Fences Found', 'berrygoodfarms' ),
		'not_found_in_trash'	=> __( 'No Fences found in Trash', 'berrygoodfarms' ),
	);
	$args = array(
		'labels'		=> $labels,
		'has_archive'	=> true,
		'public'		=> true,
		'hierarchical'	=> false,
		'menu_icon'		=> 'dashicons-portfolio',
		'rewrite'		=> array( 'slug' => 'fence' ),
		'supports'		=> array(
			'title',
			'editor',
			'excerpt',
			'custom-fields',
			'thumbnail',
			'page-attributes'
		),
		'taxonomies'	=> array( 'post_tag' ),
	);
	register_post_type( 'fence', $args );

}
add_action( 'init', 'fence_post_type' );

/**
 * Create fence post type taxonomy
 */
function register_fence_taxonomy() {

	$labels = array(
		'name'				=> __( 'Categories', 'berrygoodfarms' ),
		'singular_name'		=> __( 'Category', 'berrygoodfarms' ),
		'search_items'		=> __( 'Search Categories', 'berrygoodfarms' ),
		'all_items'			=> __( 'All Categories', 'berrygoodfarms' ),
		'edit_item'			=> __( 'Edit Category', 'berrygoodfarms' ),
		'update_item'		=> __( 'Update Category', 'berrygoodfarms' ),
		'add_new_item'		=> __( 'Add New Category', 'berrygoodfarms' ),
		'new_item_name'		=> __( 'New Category Name', 'berrygoodfarms' ),
		'menu_name'			=> __( 'Categories', 'berrygoodfarms' ),
	);

	$args = array(
		'labels'			=> $labels,
		'hierarchical'		=> true,
		'sort'				=> true,
		'args'				=> array( 'orderby' => 'term_order' ),
		'rewrite'			=> array( 'slug'    => 'fence' ),
		'show_admin_column'	=> true
	);

	register_taxonomy( 'fence_cat', array( 'fence' ), $args);

}
add_action( 'init', 'register_fence_taxonomy' );