<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Berry_Good_Farms
 * @since 1.0.0
 */

?>

</div><!-- #content -->

<footer id="colophon" class="site-footer">
	<div class="site-info container">
		<div class="footer-nav">
			<?php get_template_part('template-parts/navigation/navigation', 'bottom'); ?>
		</div>
		<div class="flex-container">
			<div class="footer-left">
				<div class="contact-info">
					<?php if (checkoption('phone')) : ?>
						<p class="phone">PHONE: <span><?php echo do_shortcode('[berrygoodfarms_option var="phone" type="link" text="" link_type="phone"]'); ?></span></p>
					<?php endif; ?>
					<?php if (checkoption('email')) : ?>
						<p class="email">EMAIL: <span><?php echo do_shortcode('[berrygoodfarms_option var="email" type="link" text="" link_type="email"]'); ?></span></p>
					<?php endif; ?>
					<?php if (checkoption('address')) : ?>
						<p class="location">LOCATION: <span><?php echo do_shortcode('[berrygoodfarms_option var="address" type="text"]'); ?></span></p>
					<?php endif; ?>
					<a href="https://goo.gl/maps/DvPGcmHsXsv" class="btn">GET <br> DIRECTIONS</a>
				</div>
				<?php get_template_part('template-parts/footer/site', 'info'); ?>
				<p class="silver"><img src="<?php echo content_url() ?>/uploads/2019/09/scnt.png" alt="Silver Connect" class="company-logo" /> <a href="https://silverconnectwebdesign.com/website-development" rel="external" target="_blank">Web Design</a> Done by <a href="https://silverconnectwebdesign.com" rel="external" target="_blank">Silver Connect Web Design</a></p>
			</div>
			<div class="footer-right">
				<?php if (checkoption('footer_logo')) :
					echo do_shortcode('[berrygoodfarms_option var="footer_logo" type="image" text="" class="footer-logo"]');
				endif; ?>
			</div>
		</div>
		<img src="<?php echo content_url() ?>/uploads/2019/09/ft-img1.png" alt="fruit" class="footer-fruit">

	</div><!-- .site-info -->
</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>