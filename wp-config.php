<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'betascxs_berrygoodfarms' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'C~1Mz5z@Tx:94{:==0]DpP<#D}J1y`*yJW^j}$2F]VylCj0}Wy%3g>^,{I:%#S!8' );
define( 'SECURE_AUTH_KEY',  '#-`wBH awe,B5o@=7F!K(*W3Mz@Wu]x}>:z9QMX >#~9fG>1]8OelwIKJfOfC &]' );
define( 'LOGGED_IN_KEY',    '`I*t89:SBMhzB-~toasm=f_|}rCW#F6:pTW9s<Hhd4@*%@{%;sTU)u>|0ix(Y*(+' );
define( 'NONCE_KEY',        '4me5o8Q8pFNy-f;Z&Or6M+T~Y[5ibE/qT_[KCccLWn=QX_2wchvW=dm+%ygbIp?V' );
define( 'AUTH_SALT',        'Wi@1+&YaK2C<&g~hRY -g%Q-OL4(NX0}e2AxC$;x]AJ[+f6NXR1orxA$orMg%(B!' );
define( 'SECURE_AUTH_SALT', '*pKZdmkM,E1(,eLqO.GQ/65Ovo_>sGjNu@zZjedt^:&^}:v,Ktq}JE+vUzgL~u[k' );
define( 'LOGGED_IN_SALT',   '=_*.2}w{WC]Bj]O9@W,!q!DD&d;bsypc,b2=*$b>L`Ew,^#]xgT2=6[~$DLQi,8K' );
define( 'NONCE_SALT',       'ox4vsTv#+S=$[/5)>sPi}$4APE?$  c;ORD:gLocI^UHeiXXh#YqM_`RTY6I!Z/Q' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'betascxs_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
